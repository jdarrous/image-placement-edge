#!/usr/bin/python
# encoding: utf-8

import functools
import json
import os
import sys

import pandas as pd


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 20)


class Layer():
    def __init__(self, id, nrep, size):
        self.id = id
        self.nrep = nrep
        self.size = size
    def inc(self):
        self.nrep += 1
    def to_tuple(self):
        return self.id, self.nrep, self.size


def merge_dfs(df_list):
    return pd.concat(df_list).copy(deep=True)

def read_dfs(pathes):
    return [pd.read_json(p, 'records') for p in pathes]

def read_df_from_dir(csv_dir):
    pathes = [os.path.join(csv_dir, n) for n in os.listdir(csv_dir)]
    pathes = pathes[:50]
    return merge_dfs(read_dfs(pathes))



def preprocessing(df):

    def memoize(f):
        memo = {}
        def helper(x):
            if x not in memo:
                memo[x] = f(x)
            return memo[x]
        return helper

    @memoize
    def parse_uri(uri):
        # <name>/[manifests|blob]/<reference> # <tag> or <digest>
        for type in ('manifests', 'blobs'):
            idx = uri.find(type)
            if idx != -1:
                name = uri[3:idx-1]
                ref = uri[idx+len(type)+1:]
                if ref.startswith('uploads'):
                    ref = ref[len('uploads')+1:]
                return name, type, ref

    # df.drop('host', axis=1, inplace=True)
    # df.drop('http.request.duration', axis=1, inplace=True)
    # df.drop('id', axis=1, inplace=True)
    # df.drop('http.response.status', axis=1, inplace=True)

    df.drop(['host','http.request.duration','id'],
            axis=1, inplace=True)

    # there is no "HEAD" request for "manifests"

    df = df[(df['http.request.useragent'].str.startswith("docker/17")) &
            (df['http.request.method'].isin(("GET", "HEAD"))) &
            (df['http.request.uri'] != "v2")].copy()

    df.drop('http.request.useragent', axis=1, inplace=True)

    # df[['http.request.uri.name',
    #     'http.request.uri.type',
    #     'http.request.uri.reference']] = \
    #         df['http.request.uri'].apply(lambda uri: pd.Series(parse_uri(uri)))

    df['http.request.uri.name'] = df['http.request.uri'].apply(lambda uri: parse_uri(uri)[0])
    df['http.request.uri.type'] = df['http.request.uri'].apply(lambda uri: parse_uri(uri)[1])
    df['http.request.uri.reference'] = df['http.request.uri'].apply(lambda uri: parse_uri(uri)[2])

    df.drop('http.request.uri', axis=1, inplace=True)

    # df['http.response.written'] /= 1024
    # df['http.response.written'] /= 1024
    return df


def gen_data(input_dir, output_dir):
    images, layers = {}, {}
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    dataset_dir = functools.partial(os.path.join, output_dir)

    for fname in os.listdir(input_dir)[:]:
        df = pd.read_json(os.path.join(input_dir, fname), 'records')
        print("+", end="", flush=True)
        df = preprocessing(df)
        print("-", end="", flush=True)

        def custom(df):
            user = df['http.request.remoteaddr'].unique()[0]
            df2 = df.sort_values('timestamp')
            m_df = df2[df2['http.request.uri.type'] == "manifests"]
            b_df = df2[df2['http.request.uri.type'] == "blobs"]
            m_cnt, b_cnt = len(m_df), len(b_df)
            if (m_cnt == 1 and b_cnt and ((m_cnt+b_cnt) == len(df2))):
                img_id = m_df['http.request.uri.reference'].values[0]
                timestamp = m_df['timestamp'].values[0]
                if img_id in images:
                    return 0
                layers_ids = list(b_df['http.request.uri.reference'].unique())
                images[img_id] = layers_ids
                for index, row in b_df.iterrows():
                    layer_id = row['http.request.uri.reference']
                    if layer_id in layers:
                        layers[layer_id].inc()
                    else:
                        layer_size = row['http.response.written']
                        layers[layer_id] = Layer(layer_id, 1, layer_size)
            return 0

        df.groupby(
            ['http.request.uri.name', 'http.request.remoteaddr']
            )[['http.request.uri.name',
               'http.request.remoteaddr',
               'http.request.uri.type',
               'http.response.written',
               'http.request.uri.reference',
               'timestamp']].apply(custom) # 'http.request.method',

    json.dump(images, open(dataset_dir('images.json'), 'w'), indent=2)
    df = pd.DataFrame([l.to_tuple() for l in layers.values()],
                      columns=['layer_id', 'layer_popularity', 'layer_size'])
    df[['layer_id', 'layer_size', 'layer_popularity']
       ].to_csv(dataset_dir('layers.csv'), index=False)

    print()
    print("NB images:", len(images))
    print("NB layers:", len(layers))


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print('usage: python dataset_extractor.py input_dir output_dir')
        print('example:')
        print(' $> python dataset_extractor.py data_centers/fra02 dataset')
        sys.exit(1)

    input_dir, output_dir = sys.argv[1:]

    gen_data(input_dir, output_dir)
