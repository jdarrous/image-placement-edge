#!venv/bin/python
# encoding: utf-8

"""
Usage:
    network_generator.py zoo <output_path>
    network_generator.py custom <nodes_per_topo> <output_path>
    network_generator.py rnd <nb_topos> <nodes_per_topo> <output_path>
    network_generator.py sample <output_path>
"""

import collections
import functools
import glob
import random
import os
import sys

from docopt import docopt
import networkx as nx

from network_utils import *


bw_decay_factor = 0.95


def to_bps(func):
    def func_wrapper(*arg):
        return func(*arg) * 8 * 1024**2
    return func_wrapper

@to_bps
def get_random_uniform_val():
    return random.randint(1, 1000)

@to_bps
def get_random_normal_val_mu_sigma(mu, sigma):
    v = int(random.normalvariate(mu, sigma))
    while (v < 1) or (v > 1000):
        v = int(random.normalvariate(mu, sigma))
    return v

@to_bps
def get_random_uniform_val_range(min, max):
    return random.randint(min, max)

@to_bps
def get_same_val(val):
    return val



def print_bw(nb_nodes, g):
    def human(bps):
        Bps = bps / 8
        if Bps < 1024: return "%dB/s" % Bps
        Bps /= 1024
        if Bps < 1024: return "%dKB/s" % Bps
        Bps /= 1024
        if Bps < 1024: return "%dMB/s" % Bps
        Bps /= 1024
        if Bps < 1024: return "%dGB/s" % Bps
    for u in range(nb_nodes):
        for v in range(nb_nodes):
            # print human(g[u][v]),
            h =  human(g[u][v])
            if u!=v and 'GB' in h:
                print ('((',h,'))',)
            else:
                print (h,)
        print


def bfs_core(graph, root):
    def visit(n):
        print(n)
    nb_nodes = len(graph.nodes())
    hops = [0] * nb_nodes
    bw = [INF_BW] * nb_nodes
    seen, queue = set([root]), collections.deque([root])
    while queue:
        vertex = queue.popleft()
        for node in graph[vertex]:
            if node not in seen:
                seen.add(node)
                queue.append(node)
                hops[node] = hops[vertex] + 1
                bw[node] = min(graph.edges[node,vertex]['weight'], bw[vertex])
    e_bw = [int(bwv * bw_decay_factor**(hopsv-1)) for bwv, hopsv in zip(bw, hops)]
    return e_bw


bfs_cache = {}
def bfs(graph, root):
    if not root in bfs_cache:
        bfs_cache[root] = bfs_core(graph, root)
    return bfs_cache[root]


def assign_bw(G, get_random_val):
    for (u, v) in G.edges():
        G.edges[u,v]['weight'] = get_random_val()
        # print(u,v, G.edges[u,v]['weight'])

def to_adjacency_matrix(G):
    global bfs_cache
    nb_nodes = len(G.nodes())
    g = [[-1] * nb_nodes for _ in range(nb_nodes)]
    for u in range(nb_nodes):
        g[u][u] = INF_BW
    bfs_cache = {}
    for u in range(nb_nodes):
        for v in range(u+1, nb_nodes):
         if g[u][v] == -1:
            g[u][v] = g[v][u] = bfs(G, u)[v]
    return g


# https://networkx.github.io/documentation/networkx-1.9.1/reference/generated/networkx.generators.random_graphs.powerlaw_cluster_graph.html#networkx.generators.random_graphs.powerlaw_cluster_graph
def gen_random_topo(nb_nodes):
    # Parameters:
    #     n : int     the number of nodes
    #     m : int     the number of random edges to add for each new node
    #     p : float,  Probability of adding a triangle after adding a random edge
    #     seed : int, optional        Seed for random number generator (default=None).
    G = nx.powerlaw_cluster_graph(nb_nodes, 2, 0.1, seed=None)
    get_random_val = functools.partial(get_random_normal_val_mu_sigma, 1000, 100)
    assign_bw(G, get_random_val)
    g = to_adjacency_matrix(G)
    return g


def _graph_from_elist(elist):
    G = nx.Graph()
    G.add_weighted_edges_from(elist)
    return to_adjacency_matrix(G)


def topology_zoo(input_gml_file):
    print(input_gml_file)
    g = nx.read_gml(input_gml_file)

    # external_nodes = [nodeid for nodeid, nodedata in g.nodes(data=True)
    #                             if not nodedata['Internal']]
    # print(external_nodes)
    internal_nodes = [nodeid for nodeid, nodedata in g.nodes(data=True)
                                if nodedata['Internal']]
    mp = {name: idx for idx, name in enumerate(internal_nodes)}

    def get_agg_link_speed(u, v):
        # return sum([int(g.edges[u,v,k].get('LinkSpeedRaw', 0))
        #             for k in range(g.number_of_edges(u, v))])
        return sum([int(g.edges[u,v,k]['LinkSpeedRaw'])
                    for k in range(g.number_of_edges(u, v))])

    def get_single_link_speed(u, v):
        # return int(g.edges[u,v].get('LinkSpeedRaw', 0))
        return int(g.edges[u,v]['LinkSpeedRaw'])

    get_link_speed = get_agg_link_speed if isinstance(g, nx.MultiGraph) else get_single_link_speed

    elist = [(mp[u], mp[v], get_link_speed(u, v))
                for (u, v) in set(g.edges())
                    if u in internal_nodes and v in internal_nodes]

    return _graph_from_elist(elist)


def gen_complete_topo_func(nb_nodes, get_random_val):
    elist = [(i, j, get_random_val())
                for i in range(nb_nodes)
                    for j in range(i+1, nb_nodes)]
    return _graph_from_elist(elist)


def sample_topology():
    f=8 * (1024**2)
    elist = [(0, 1, 20*f), (0, 2, 20*f), (0, 3, 20*f), (0, 4, 100*f),
             (4, 1+4, 20*f), (4, 2+4, 20*f), (4, 3+4, 20*f), (4, 8, 100*f),
             (8, 1+8, 20*f), (8, 2+8, 20*f), (8, 3+8, 20*f), (8, 0, 100*f)]
    yield 'c-star', _graph_from_elist(elist)
    elist = [(0, 1, 10*f), (0, 2, 10*f), (0, 3, 10*f), (0, 4, 10*f),
             (5, 1, 10*f), (6, 2, 10*f), (7, 3, 10*f), (8, 4, 10*f),]
    yield 'star', _graph_from_elist(elist)
    elist = [(0, 1, 10*f), (1, 2, 10*f), (2, 3, 10*f), (3, 4, 10*f),
             (4, 5, 10*f), (5, 6, 10*f), (6, 7, 10*f), (7, 8, 10*f)]
    yield 'line', _graph_from_elist(elist)



def gen_random_topos(nb_topos, nodes_per_topo, output_path):
    write_network_to_file({
        "topo-%d" % i: gen_random_topo(nodes_per_topo)
            for i in range(nb_topos)}, output_path)

def gen_topology_zoo(output_path):
    write_network_to_file({
        os.path.basename(path).split('.')[0]: topology_zoo(path)
            for path in glob.glob('topology-zoo/candidates/with-link-speed/*.gml')}, output_path)

def gen_sample_topos(output_path):
    write_network_to_file({key: g for key, g in sample_topology()}, output_path)


def gen_custom_topos(nb_nodes, output_path):

    def N(mu, sigma):
        return functools.partial(get_random_normal_val_mu_sigma, mu, sigma)

    def U(min, max):
        return functools.partial(get_random_uniform_val_range, min, max)

    def C(val):
        return functools.partial(get_same_val, val)

    def gen_complete(nb_nodes):
        return functools.partial(gen_complete_topo_func, nb_nodes)

    gen = gen_complete(nb_nodes)

    write_network_to_file({
        'v-low-bw'  : gen(N(10, 300)),
        'low-bw'    : gen(N(150, 250)),
        'med-1-bw'  : gen(N(500, 25)),
        'med-2-bw'  : gen(N(500, 125)),
        'med-3-bw'  : gen(N(500, 250)),
        'high-bw'   : gen(N(850, 250)),
        'v-high-bw' : gen(N(990, 300)),
        'homo-bw'   : gen(C(500)),
        'uni-bw'    : gen(U(1, 1000)),
        }, output_path)



if __name__ == "__main__":

    # usage:
    # python scripts/network_generator.py zoo networks/topology_zoo.json
    # python scripts/network_generator.py custom 50 networks/custom.json
    # python scripts/network_generator.py rnd 5 100 networks/5t_100n_n.json
    # python scripts/network_generator.py sample networks/c.json

    args = docopt(__doc__, version='1.0.0')

    output_path = args["<output_path>"]

    if args["rnd"]:
        nb_topos, nodes_per_topo = int(args["<nb_topos>"]), int(args["<nodes_per_topo>"])
        gen_random_topos(nb_topos, nodes_per_topo, output_path)

    if args["zoo"]:
        gen_topology_zoo(output_path)

    if args["custom"]:
        gen_custom_topos(int(args["<nodes_per_topo>"]), output_path)

    if args["sample"]:
        gen_sample_topos(output_path)

    sys.exit(0)
