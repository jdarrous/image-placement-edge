#!/usr/bin/python
# encoding: utf-8

import functools
import sys

import numpy as np

from network_utils import *


def human(bps):
    if bps < 1000: return "%dbps" % bps
    bps /= 1000.0
    if bps < 1000: return "%dKbps" % bps
    bps /= 1000.0
    if bps < 1000: return "%dMbps" % bps
    bps /= 1000.0
    if bps < 1000: return "%dGbps" % bps


def network_bw_stats(network):
    all_bw = []
    for row in network:
        all_bw.extend(row)
    all_bw = [bw for bw in all_bw if bw != INF_BW]
    all_bw = [bw for bw in all_bw if bw]
    vals = (np.mean(all_bw), min(all_bw),
            np.percentile(all_bw, 25),
            np.median(all_bw),  np.percentile(all_bw, 75),
            max(all_bw))
    print("%8s "*len(vals) % tuple([human(v) for v in vals]))


def print_network_stats(networks):
    print("network     nb nodes     mean     min     25th    median   75th       max")
    for network_id, network in sorted(networks.items(), key=lambda x: x[0]):
        print("%12s %6d " % (network_id, len(network)), end="")
        network_bw_stats(network)


def print_network_stats_per_node(networks):

    def compare(a, b):
        for va, vb in zip(a[1][::-1], b[1][::-1]):
            if va > vb:
                return -1
            if va < vb:
                return +1
        return 0

    for network_id, network in sorted(networks.items(), key=lambda x: x[0]):
        print(network_id, len(network))
        nodes = [(nid, sorted(bw[:nid]+bw[nid+1:]))
                    for nid, bw in enumerate(network)]
        nodes.sort(key=functools.cmp_to_key(compare))
        print("id    min     25th   median   75th     max")
        for node in nodes:
            nodeid, nodebw = node
            vals = (min(nodebw), np.percentile(nodebw, 25),
                    np.median(nodebw), np.percentile(nodebw, 75), max(nodebw))
            bw_str = " %7s"*len(vals) % tuple([human(v) for v in vals])
            print("%2d %s" % (nodeid, bw_str))
        print()


def main(network_filepath):

    networks = load_network_from_file(network_filepath)

    print_network_stats(networks)
    # print_network_stats_per_node(networks)


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print('usage: python network_stats.py network_filepath')
        print('example:')
        print(' $> python network_stats.py networks/custom.json')
        print(' $> python network_stats.py networks/topology_zoo.json')
        sys.exit(1)

    network_filepath = sys.argv[1]

    main(network_filepath)
