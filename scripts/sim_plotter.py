#!venv/bin/python
# encoding: utf-8

"""
Usage:
    sim_plotter.py (layer|image) <input_dir> <output_dir> [-b] [--wc]
"""

import functools
import glob
import itertools
import os
import sys

from docopt import docopt
import pandas as pd
import matplotlib.pyplot as plt


# compare between different f factors
withoutconflict_flag = 0
# consider just the best performance
# for BestFit and Random
best_bf_rnd_flag = 0


############################
# utils
############################

def get_rel_path(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return functools.partial(os.path.join, path)

def itr_df_props(df, props, filters=None):
    if filters is None:
        filters = [None] * range(len(props))
    def select_list(filter_func, lst):
        return [c for c in lst if not filter_func(c)] \
                if filter_func is not None else lst
    def select_list_df(filter_func, df, prop):
        return select_list(filter_func, sorted(getattr(df, prop).unique()))
    select_lists = [select_list_df(filter, df, prop)
                    for prop, filter in zip(props, filters)]
    for select_vals in itertools.product(*select_lists):
        dd = functools.reduce(lambda f, p: f[(getattr(f, p[1]) == p[0])],
                zip(select_vals, props), df)
        yield (dd, *select_vals)


############################
# Helpers
############################

def save_close(fig, output_image_path, **kwargs):
    if fig is None: return
    dpi = kwargs.get("dpi", 100)
    format = kwargs.get("format", "pdf")
    fontsize = kwargs.get("fontsize", 20)
    ticks_fontsize = kwargs.get("ticks_fontsize", fontsize)
    def format_ax(ax):
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(ticks_fontsize)
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label]):
            item.set_fontsize(fontsize)
    for ax in fig.get_axes():
        format_ax(ax)
    fig.tight_layout()
    plt.tight_layout(pad=1.0, w_pad=0.5, h_pad=5.0)
    fig.savefig(output_image_path + '.' + format, format=format, dpi=dpi)
    plt.close(fig)

def hatch_cdfs(ax):
    lines = ax.patches
    # linestyle or ls: ['solid' , 'dashed', 'dashdot', 'dotted']
    linestyle = ['solid' , 'dashed', 'dashdot', 'dotted'] * 2
    linewidth = (2.0, ) * len(lines)
    for line, ls, lw in zip(lines, linestyle, linewidth):
        line.set_lw(lw)
        line.set_ls(ls)

def hatch_bars(df, ax):
    bars = ax.patches
    nb_bars, nb_groups = len(bars), len(df)
    nb_columns = nb_bars // nb_groups
    # print(nb_bars, nb_groups , nb_columns)
    linewidth= (1.1, ) * nb_bars
    patterns = ('//','\\\\', '-','+', 'o','O', 'x', '\\','/')[:nb_columns]
    hatches = [p for p in patterns for i in range(nb_groups)]
    for bar, hatch, lw in zip(bars, hatches, linewidth):
        bar.set_hatch(hatch)
        bar.set_lw(lw)
        # bar.set_color('white')
        bar.set_edgecolor('black')


############################
# Plotting/Figures
############################

def plot_placement_by_repf(df):
    df = df.copy()
    color = 'Pastel2'
    grby = df.groupby(['placement', 'repf'])['max']
    mean, errors = grby.mean().unstack(), grby.std().unstack()
    if best_bf_rnd_flag:
        mean, errors = grby.min().unstack(), None
    ax = mean.plot.bar(rot=0, yerr=errors, cmap=color,
        error_kw=dict(ecolor='k', capsize=3, elinewidth=1, markeredgewidth=1))#, figsize=(7.2, 7.2)
    ax.set_ylabel("Retrieval time (sec)")
    ax.set_xlabel("Placement strategy")
    hatch_bars(mean, ax)
    ax.legend(["No replication", "3 replicas"], loc='best', fontsize='xx-large')
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_placement_by_cap_scaling(df):
    df = df.copy()
    df['cap_scaling'] = df['cap_scaling'].apply(lambda x: x if x<4 else 'INF')
    color = 'Pastel2'
    if withoutconflict_flag:
        xlabel = "Large layers threshold (f-factor)"
    else:
        xlabel = "Placement strategy"
    ylabel = "Retrieval time (sec)"
    grby = df.groupby(['placement', 'cap_scaling'])['max']
    mean, errors = grby.mean().unstack(), grby.std().unstack()
    if best_bf_rnd_flag:
        mean, errors = grby.min().unstack(), None
    ax = mean.plot.bar(rot=0, yerr=errors, cmap=color, figsize=(6.4, 6.4, ),
        error_kw=dict(ecolor='k', capsize=3, elinewidth=1, markeredgewidth=1))#, figsize=(7.2, 7.2)
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    hatch_bars(mean, ax)
    ax.legend(title='Capacity scaling', ncol=4, loc='upper center',
                fontsize='xx-large', bbox_to_anchor=(0.5, 1.25))
    ax.get_legend().get_title().set_fontsize('20')
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_rtime_cdf(df, dimension, **kwargs):
    df['placement'] = df['placement'].apply(lambda x: x.replace('\n', ' '))
    grby = df.groupby(['placement', dimension])['rtime'].max().reset_index()
    # grby = grby[grby['rtime'] > 10]
    pdf = grby.pivot(columns='placement', values='rtime')
    ax = pdf.plot.hist(cumulative=True, density=1, bins=500, histtype='step')
    ax.set_xlabel('Retrieval time (sec)')
    ax.set_ylabel('')
    hatch_cdfs(ax)
    ax.legend(title='', loc='lower right', fontsize='xx-large')
    ax.set_title('')
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_by_network_id(df, output_dir):
    awd = get_rel_path(output_dir)

    df = df.copy()
    df['max'] = df['max'].apply(lambda x: 0 if x < 0 else x)

    for dfd, repf in itr_df_props(df, ['repf'], [lambda r: 0]):
        if not len(dfd): continue
        save_close(
            plot_placement_by_cap_scaling(dfd),
            awd("%drep-max" % (repf, )))

    # for dfd, cap_scaling in itr_df_props(df, ['cap_scaling'], [lambda r: r!=2]):
    #     if not len(dfd): continue
    #     save_close(
    #         plot_placement_by_repf(dfd),
    #         awd("%dcap_scaling-max" % (cap_scaling, )))


def plot_v2(df, output_dir):
    awd = get_rel_path(output_dir)

    main_cols = ['iter', 'network_id', 'nb_nodes', 'repf', 'cap_scaling', 'placement']
    dft = df.groupby(main_cols)['rtime'].count().reset_index()
    dft_max = df.groupby(main_cols)['rtime'].max().reset_index()
    dft_mean = df.groupby(main_cols)['rtime'].mean().reset_index()
    dft_median = df.groupby(main_cols)['rtime'].median().reset_index()
    dft_q90 = df.groupby(main_cols)['rtime'].quantile(.9).reset_index()
    dft_q95 = df.groupby(main_cols)['rtime'].quantile(.95).reset_index()
    dft_q99 = df.groupby(main_cols)['rtime'].quantile(.99).reset_index()
    dft['max'] = dft_max['rtime']
    dft['mean'] = dft_mean['rtime']
    dft['median'] = dft_median['rtime']
    dft['90th'] = dft_q90['rtime']
    dft['95th'] = dft_q95['rtime']
    dft['99th'] = dft_q99['rtime']

    # plot_by_network_id(dft, output_dir)
    # return

    for dfd, repf, cap_scaling in itr_df_props(df,
            ['repf', 'cap_scaling'],
            [lambda r: 0, lambda c: 0]):
            # [lambda r: r!=3, lambda c: c!=3]):
        if not len(dfd): continue
        save_close(
            plot_rtime_cdf(dfd, 'node'),
            awd("%drep-%dcap-cdf-per-node-max" % (repf, cap_scaling)))
        save_close(
            plot_rtime_cdf(dfd, 'imageid'),
            awd("%drep-%dcap-cdf-per-image-max" % (repf, cap_scaling)))


def plot(csv_path, output_dir):
    awd = get_rel_path(output_dir)

    df = pd.read_csv(csv_path)

    mp = {
            'KcenterStrategy':     '1\nKCBP',
            'KcenterWCStrategy':   '2\nKCBP-WC',
            'BestFitStrategy':     '3\nBest-Fit',
            'RandomStrategy':      '4\nRandom'}

    # print('iter        :', len(list(df.iter.unique())))
    # print('repf        :', list(df.repf.unique()))
    # print('cap scaling :', list(df.cap_scaling.unique()))
    # print('placement   :', list(df.placement.unique()))
    # print('t_percent   :', list(df.t_percent.unique()))
    # print('network_id  :', list(df.network_id.unique()))
    # return

    df = df[df['cap_scaling'].isin((1.1, 2,)) | (df['cap_scaling'] > 3)].copy()

    df['placement'] = df['placement'].replace(mp)

    if withoutconflict_flag:
        df = df[df['placement'] == mp['KcenterWCStrategy']].copy()
        df['placement'] = df['t_percent'].apply(lambda x:'%02d%%'%(x*100,))
    elif 1:
        df = df[df['t_percent'] == 0.10].copy()
        # df = df[~((df['t_percent'] != 0.05) & (df['placement'] == mp['KcenterWCStrategy']))].copy()
        pass
    else:
        fmt = lambda x: x[0] if x[0] != mp['KcenterWCStrategy'] else '%s(%02d%%)' % (x[0], x[1]*100)
        df['placement'] = df[['placement', 't_percent']].apply(fmt, axis=1)


    outdir_name = os.path.basename(csv_path).split('.')[0]
    awd = get_rel_path(awd(outdir_name))
    for network_id in df.network_id.unique():
        if 'topology_zoo' in csv_path:
            if not network_id in ('Renater2010', 'Sanet'):
                continue
        if 'custom' in csv_path:
            if not network_id in ('homo-bw', 'v-high-bw', 'v-low-bw', 'uni-bw'):
                continue
        print('>>', network_id)
        df_network_id = df[(df.network_id == network_id)]
        if 'imageid' in df.columns:
            plot_v2(df_network_id, awd(network_id))
        else:
            plot_by_network_id(df_network_id, awd(network_id))


def main(retrieval_level, input_dir, output_dir):
    pattern = "%s/%s-level-*.csv*" % (input_dir, retrieval_level)
    for csvpath in glob.glob(pattern):
        plot(csvpath, output_dir)


if __name__ == "__main__":

    args = docopt(__doc__, version='1.0.0')

    if args["layer"]:
        retrieval_level = "layer"
    if args["image"]:
        retrieval_level = "image"

    input_dir = args["<input_dir>"]
    output_dir = args["<output_dir>"]

    best_bf_rnd_flag = args["-b"]
    withoutconflict_flag = args["--wc"]

    main(retrieval_level, input_dir, output_dir)
