""" The driver module and the entry point of the simulator.

    Takes as input:
     - the yaml configuration file
     - the output result directory
"""


import multiprocessing
import os
import sys


def _load_yaml_file(yaml_filepath):
    import yaml
    with open(yaml_filepath, 'r') as stream:
        try:
            return yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            raise exc


def sim(output_dir, config_file):

    if os.path.exists(output_dir):
        print('output_dir: [%s] is already exists.' % output_dir)
        sys.exit(1)

    os.mkdir(output_dir)

    config = _load_yaml_file(config_file)

    if config['output_format'] == 'per_placement':
        from sim_concise import run_expr
    elif config['output_format'] == 'per_image_node':
        from sim_complete import run_expr
    else:
        print('output format [%s] is not recognized.' % config['output_format'])
        return 1

    networks_file_id_list = config['networks_file_id_list']
    retrieval_level_list = config['retrieval_level_list']

    proc = [multiprocessing.Process(
                name='run-%s-%s' % (networks_file_id, retrieval_level),
                target=run_expr,
                args=(networks_file_id, retrieval_level, output_dir, config))
            for networks_file_id in networks_file_id_list
            for retrieval_level in retrieval_level_list]

    for p in proc:
        p.start()
    for p in proc:
        p.join()

    print('all processes are finished.')
    return 0


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print('usage: python sim.py output_dir config_file')
        sys.exit(1)

    sys.exit(sim(*sys.argv[1:]))
