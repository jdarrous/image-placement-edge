"""A helper module to store data in a dataframe like model

    It's used to store simulation results.
"""

import gzip


class Dataframe():
    """Simple Dataframe class that give its content as csv format"""
    def __init__(self, header):
        self.rows = list()
        self.add_row(*header)
    def add_row(self, *values):
        self.rows.append(values)
        if len(values) != len(self.rows[0]):
            print("WARNING: Dataframe: header length [%d] and row length [%d] "
                  "mismatched)" % (len(self.rows[0]), len(values)))
    def to_csv(self):
        return [','.join(map(str, row)) for row in self.rows]

    # @staticmethod
    # def dummy():
    #     df = Dataframe(('None'))
    #     del df.rows
    #     df.add_row = lambda *values: None
    #     df.to_csv = lambda: ''
    #     return df



class BigDataframe():
    """Handle big Dataframes by flushing data to disk and use compressed files"""
    def __init__(self, file, header):
        self.file = file + '.gz'
        self.rows = list()
        self.nb_cols = len(header)
        self.add_row(*header)
        with gzip.open(self.file, 'w') as fout:
            fout.write('')
    def add_row(self, *values):
        self.rows.append(values)
        if len(values) != self.nb_cols:
            print("WARNING: Dataframe: header length [%d] and row length [%d] "
                  "mismatched)" % (self.nb_cols, len(values)))
    def to_csv(self):
        return [','.join(map(str, row)) for row in self.rows]
    def flush(self):
        if not self.rows:
            return
        content = '\n'.join(self.to_csv())
        self.rows = list()
        with gzip.open(self.file, 'a') as fout:
            fout.write(content)
            fout.write('\n')
