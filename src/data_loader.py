"""A helper module to load images dataset and networks files
"""

import json
import os

from model import Layer


def load_layers(dataset_dir):
    """Load layers file ('layers.csv') from the given `dataset_dir` directory"""
    def parse_layer(layer):
        lid, lsize, lrep = layer.split(',')
        return Layer(lid, int(lrep), int(float(lsize)))
    lpath = os.path.join(dataset_dir, 'layers.csv')
    return [parse_layer(line) for line in open(lpath).read().split()[1:]]

def load_images(dataset_dir):
    """Load images file ('images.json') from the given `dataset_dir` directory"""
    ipath = os.path.join(dataset_dir, 'images.json')
    return json.load(open(ipath))

def load_network_from_file(networks_dir, fname):
    """Load a network file from the given `networks_dir` directory"""
    npath = os.path.join(networks_dir, fname + '.json')
    return json.load(open(npath))
