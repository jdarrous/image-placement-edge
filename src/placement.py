"""This module contains everything related to placement.

    It includes the `Placement` class and the `PlacementStrategy`
    implementations
"""

import abc
import random

from k_center import KCenterSrcSolver



class Placement():
    """The placement object. Holds the mapping between layers and nodes."""
    def __init__(self, nb_layers, nb_nodes):
        self._mp = dict()
        self._nmp = None
        self.nb_layers = nb_layers
        self.nb_nodes = nb_nodes
    def init(self, layer):
        self._mp[layer] = list()
    def add(self, layer, nodes):
        self._mp[layer] = nodes
    def append(self, layer, node):
        self._mp[layer].append(node)
    def get_nodes(self, layer):
        return self._mp.get(layer, list())
    def is_in(self, layer, node):
        return node in self.get_nodes(layer)

    def inverse_mapping(self):
        if self._nmp is None:
            upn = [list() for _ in range(self.nb_nodes)]
            for layer, assign in self._mp.items():
                for node in assign:
                    upn[node].append(layer)
            self._nmp = upn
        return self._nmp

    def verify(self):
        if self.nb_layers != len(self._mp):
            return False
        for layer, replica in self._mp.items():
            if len(set(replica)) == len(replica) and len(replica) == layer.nrep:
                continue
            print(layer)
            return False
        return True

    def __str__(self):
        return  "\n########################################\n" + \
                "\n".join(["%r: %r" % (layer, replica)
                           for layer, replica in self._mp.items()]) + \
                "\n----------------------------------------\n" + \
                "\n".join(["node(id=%r): %r" % (node, [l.id for l in ls])
                           for node, ls in enumerate(self.inverse_mapping())]) + \
                "\n########################################\n"

    def __repr__(self):
        return str(self)




class PlacementStrategy():
    """The abstract placement strategy class."""
    __metaclass__ = abc.ABCMeta
    def __init__(self, images, layers, infrastructure, **kwargs):
        self.images = images
        self.layers = layers
        self.infrastructure = infrastructure
    def layers_sorted_by_size(self):
        return sorted(self.layers, key=lambda layer: layer.size, reverse=True)
    def new_placement(self):
        return Placement(len(self.layers), self.infrastructure.nb_nodes)
    def new_placement_if_none(self, placement):
        return self.new_placement() if placement is None else placement
    @abc.abstractmethod
    def place(self, placement=None):
        pass




class RandomStrategy(PlacementStrategy):
    """Randomly assign layers to nodes."""
    def place(self, placement=None):
        placement = self.new_placement_if_none(placement)
        nb_nodes = self.infrastructure.nb_nodes
        capacities = self.infrastructure.capacities
        for layer in self.layers:
            available = [i for i in range(nb_nodes) if capacities[i] >= layer.size]
            if len(available) < layer.nrep:
                print("ERROR: can't place all the replica of layer", layer)
                return False, placement
            nodes = random.sample(available, layer.nrep)
            placement.add(layer, nodes)
            for node in nodes:
                capacities[node] -= layer.size
        return True, placement



class BestFitStrategy(PlacementStrategy):
    """A Greedy algorithm to assign layers to nodes.

        Assign layers (from largest to the smallest) to
        nodes with the highest remaining empty space.
    """
    class Node():
        def __init__(self, n_id, capacity):
            self.n_id = n_id
            self.rem = capacity
        def can_add_layer(self, layer):
            return self.rem >= layer.size
        def add_layer(self, layer):
            self.rem -= layer.size

    def place(self, placement=None):
        placement = self.new_placement_if_none(placement)
        nodes_cap = zip(self.infrastructure.nodes(), self.infrastructure.capacities)
        nodes = [self.__class__.Node(node_id, node_cap)
                 for node_id, node_cap in nodes_cap]
        random.shuffle(nodes)
        for layer in self.layers_sorted_by_size():
            nodes = sorted(nodes, key=lambda node: node.rem, reverse=True)
            rep = layer.nrep
            rep_nodes = list()
            for node in nodes:
                if node.can_add_layer(layer):
                    node.add_layer(layer)
                    rep_nodes.append(node)
                    rep -= 1
                    if not rep:
                        break
            if rep:
                print("ERROR: can't place all the replica of layer", layer)
                return False, placement
            placement.add(layer, [node.n_id for node in rep_nodes])
        return True, placement



class AbstractKcenterStrategy(PlacementStrategy):
    """Abstract class for k-Center based algorithms."""
    __metaclass__ = abc.ABCMeta
    def _place_impl(self, impl, placement=None):
        placement = self.new_placement_if_none(placement)
        for layer in self.layers_sorted_by_size():
            centers = impl(self.infrastructure.capacities,
                           self.infrastructure.network,
                           layer.size, layer.nrep)
            if len(centers) < layer.nrep:
                return False, placement
            placement.add(layer, centers)
        return True, placement



class KcenterStrategy(AbstractKcenterStrategy):
    """The default k-Center implementation."""
    def place(self, placement=None):
        return self._place_impl(KCenterSrcSolver, placement)


class KcenterWRStrategy(PlacementStrategy):
    """The k-Center with Random implementation.

        Apply k-Center for the largest 10% of the layers
        and then assign the remaining ones randomly.
    """
    def place(self, placement=None):
        placement = self.new_placement_if_none(placement)
        s_layers = self.layers_sorted_by_size()
        split_index = int(len(s_layers) / 10.0)
        big_layers, other_layers = s_layers[:split_index], s_layers[split_index:]
        kcentr_placement = KcenterStrategy(None, big_layers, self.infrastructure)
        is_valid, placement = kcentr_placement.place(placement)
        if not is_valid:
            return is_valid, placement
        random_placement = RandomStrategy(None, other_layers, self.infrastructure)
        is_valid, placement = random_placement.place(placement)
        return is_valid, placement



class KcenterWCStrategy(PlacementStrategy):
    """The k-Center Without Conflict implementation.

        Do not place two large layer of the same image on the same node.
    """
    def __init__(self, images, layers, infrastructure, **kwargs):
        super(KcenterWCStrategy, self).__init__(images, layers, infrastructure)
        self.big_layer_threshold = kwargs["big_layer_threshold"]

        mp_layer_id = {layer.id: layer for layer in layers}
        self.images_layers = {imageid: map(mp_layer_id.get, im_layers_ids)
                              for imageid, im_layers_ids in images.iteritems()}
        self.layer_images = {}
        for imageid, im_layers in self.images_layers.iteritems():
            for layer in im_layers:
                current_images = self.layer_images.get(layer, list())
                self.layer_images[layer] = current_images + [imageid]

    def _isbig(self, layer):
        return layer.size >= self.big_layer_threshold

    def place(self, placement=None):
        return self._place_impl(KCenterSrcSolver, placement)

    def _get_sibling_big_layers(self, layer):
        return set([im_layer
                    for image_id in self.layer_images[layer]
                    for im_layer in self.images_layers[image_id]
                    if (layer.id != im_layer.id) and self._isbig(im_layer)])

    def _save_state(self, layer, placement, capacities):
        if not self._isbig(layer):
            return list()
        cap_layer_images = [0] * len(capacities)
        for s_layer in self._get_sibling_big_layers(layer):
            for node_id in placement.get_nodes(s_layer):
                cap_layer_images[node_id] += s_layer.size
        max_cap_per_image = max(cap_layer_images)
        if not max_cap_per_image:
            return list()
        removed_node_cap = list()
        for node_id, cap in enumerate(cap_layer_images):
            if cap + layer.size > max_cap_per_image:
                removed_node_cap.append((capacities[node_id], node_id))
                capacities[node_id] = 0
        return removed_node_cap

    def _reload_state(self, removed_node_cap, capacities):
        for node_cap, node_id in removed_node_cap:
            capacities[node_id] = node_cap

    def _place_impl(self, impl, placement=None):

        placement = self.new_placement_if_none(placement)
        capacities = self.infrastructure.capacities
        bandwidth = self.infrastructure.network

        for layer in self.layers_sorted_by_size():
            state = self._save_state(layer, placement, capacities)
            centers = impl(capacities, bandwidth, layer.size, layer.nrep)
            if len(centers) < layer.nrep:
                return False, placement
            placement.add(layer, centers)
            self._reload_state(state, capacities)
        return True, placement
