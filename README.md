# Container image placement in Edge clouds

A Python simulator for container image placement and retrieval algorithms in Edge Clouds.

*For more details, please see our [paper](https://hal.inria.fr/hal-02134507) at ICCCN 2019*

## Prepare dev environment
All scripts in `scripts` directory are written in python 3.6, while, the simulator code is written in python 2.7, and for performance reason is better to be run with PyPy.

Create a virtual environment for data preparation and analysis code
```sh
virtualenv -p /path/to/python3.6 venv3.6
. venv3.6/bin/activate
pip install -r requirements_scripts.txt
```

Create another virtual environment for the simulator code
```sh
virtualenv -p /path/to/pypy2.7-v7.0.0-*/bin/pypy venvpypy2.7
. venvpypy2.7/bin/activate
pip install -r requirements_src.txt
```

## Prepare simulation data (Optional)
Image dataset and networks are already available in `dataset` and `networks` directories, respectively.

### Extract images dataset
Traces are retrieved from [Improving Docker Registry Design based on Production Workload Analysis, FAST 2018](https://dssl.cs.vt.edu/drtp/).
We extract the images and their corresponding layers from the traces of `frankfort` data center.
```sh
python scripts/dataset_extractor.py data_centers/fra02 dataset
```

Plot the dataset
```sh
python scripts/dataset_plotter.py dataset plots-dir
```

### Generate/Extract networks
To generate custom/random network topologies
```sh
python scripts/network_generator.py custom 50 networks/custom.json
```
Or create network topologies from real-world networks, i.e., network data from [topology-zoo](http://topology-zoo.org/) should be already downloaded.
```sh
python scripts/network_generator.py zoo networks/topology_zoo.json
```

Show network links characteristics
```sh
python scripts/network_stats.py networks/topology_zoo.json
```

## Run the simulation
The simulator configurations are described in a yaml file.
The file [`expr.config.yaml`](expr.config.yaml) contains the complete parameters, while [`sample.config.yaml`](sample.config.yaml) can be used for testing.

Then to launch the simulator, simply type
```sh
python src/sim.py results-dir sample.config.yaml
```

## Analyse the simulation results
The output of the simulator is a csv file that can be read and analyzed as follow
```sh
python scripts/sim_plotter.py image results-dir plots-dir
```
